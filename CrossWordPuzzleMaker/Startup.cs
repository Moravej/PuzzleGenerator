﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CrossWordPuzzleMaker.Startup))]
namespace CrossWordPuzzleMaker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
